# Top level CMakeLists.txt file for UFC

# Require CMake 2.6
cmake_minimum_required(VERSION 2.6)

#------------------------------------------------------------------------------
# Set project name and version number

project(UFC NONE)
set(UFC_VERSION_MAJOR "2")
set(UFC_VERSION_MINOR "4")
set(UFC_VERSION_MICRO "0")
set(UFC_VERSION_STRING "${UFC_VERSION_MAJOR}.${UFC_VERSION_MINOR}.${UFC_VERSION_MICRO}")


# Set verbose output while testing CMake
set(CMAKE_VERBOSE_MAKEFILE 1)
message(STATUS "Install prefix: ${CMAKE_INSTALL_PREFIX}")

# Set location of our FindFoo.cmake modules
set(UFC_CMAKE_DIR "${UFC_SOURCE_DIR}/cmake" CACHE INTERNAL "")
set(CMAKE_MODULE_PATH "${UFC_CMAKE_DIR}/modules")

#------------------------------------------------------------------------------
# Options

option(UFC_ENABLE_PYTHON "Enable Python extensions." FALSE)

#------------------------------------------------------------------------------
# Run tests to find required packages

if(UFC_ENABLE_PYTHON)
message(STATUS "Python bindings are enabled")
#===DEADZONE===================================================================
# Set special link option, see `cmake --help-policy CMP0003`
if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif()
find_package(PythonInterp QUIET)
# Find Python library corresponding to Python interpreter
find_package(PythonLibs ${PYTHON_VERSION_STRING} EXACT QUIET)
message(STATUS ${PYTHON_INCLUDE_PATH})

#------------------------------------------------------------------------------
# Get installation path for Python modules

# Get Python module path from distutils
if (PYTHONINTERP_FOUND)

  if (NOT DEFINED UFC_INSTALL_PYTHON_EXT_DIR)
    # Get path for platform-dependent Python modules (since we install a binary libary)
    execute_process(
      COMMAND ${PYTiHON_EXECUTABLE} -c "import sys, distutils.sysconfig; sys.stdout.write(distutils.sysconfig.get_python_lib(plat_specific=True, prefix='${CMAKE_INSTALL_PREFIX}'))"
      OUTPUT_VARIABLE UFC_INSTALL_PYTHON_EXT_DIR
      )
    # Strip off CMAKE_INSTALL_PREFIX (is added later by CMake)
    string(REGEX REPLACE "${CMAKE_INSTALL_PREFIX}(/|\\\\)([^ ]*)" "\\2"
      UFC_INSTALL_PYTHON_EXT_DIR "${UFC_INSTALL_PYTHON_EXT_DIR}")
    set(UFC_INSTALL_PYTHON_EXT_DIR ${UFC_INSTALL_PYTHON_EXT_DIR}
      CACHE PATH "Python extension module installation directory.")
  endif()

  if (NOT DEFINED UFC_INSTALL_PYTHON_MODULE_DIR)
    # Get path for pure Python modules
    execute_process(
      COMMAND ${PYTHON_EXECUTABLE} -c "import sys, distutils.sysconfig; sys.stdout.write(distutils.sysconfig.get_python_lib(plat_specific=False, prefix='${CMAKE_INSTALL_PREFIX}'))"
      OUTPUT_VARIABLE UFC_INSTALL_PYTHON_MODULE_DIR
      )
    # Strip off CMAKE_INSTALL_PREFIX (is added later by CMake)
    string(REGEX REPLACE "${CMAKE_INSTALL_PREFIX}(/|\\\\)([^ ]*)" "\\2"
      UFC_INSTALL_PYTHON_MODULE_DIR "${UFC_INSTALL_PYTHON_MODULE_DIR}")
    set(UFC_INSTALL_PYTHON_MODULE_DIR ${UFC_INSTALL_PYTHON_MODULE_DIR}
      CACHE PATH "Python module installation directory.")
  endif()
endif (PYTHONINTERP_FOUND)
#===DEADZONE===================================================================
endif(UFC_ENABLE_PYTHON)

#------------------------------------------------------------------------------
# Target names and installation directories

# Set UFC install sub-directories
if (LIB_INSTALL_DIR)
  set(UFC_LIB_DIR "${LIB_INSTALL_DIR}" CACHE PATH "Library installation directory.")
else()
  set(UFC_LIB_DIR "lib" CACHE PATH "Library installation directory.")
endif()
set(UFC_INCLUDE_DIR "include" CACHE PATH "C/C++ header installation directory.")
set(UFC_PKGCONFIG_DIR "${UFC_LIB_DIR}/pkgconfig" CACHE PATH "pkg-config file installation directory.")
set(UFC_CMAKE_CONFIG_DIR "share/ufc" CACHE PATH "CMake configuration file directory.")

#------------------------------------------------------------------------------
# Install ufc.h

set(UFC_H src/ufc/ufc.h)
set(UFC_GEOMETRY_H src/ufc/ufc_geometry.h)
install(FILES ${UFC_H} DESTINATION ${UFC_INCLUDE_DIR} COMPONENT Development)
install(FILES ${UFC_GEOMETRY_H} DESTINATION ${UFC_INCLUDE_DIR} COMPONENT Development)

#------------------------------------------------------------------------------
# Install Python utils (ufc_utils)

if(UFC_ENABLE_PYTHON)
#===DEADZONE===================================================================

install(DIRECTORY
  src/utils/python/ufc_utils
  DESTINATION ${UFC_INSTALL_PYTHON_MODULE_DIR}
  USE_SOURCE_PERMISSIONS
  COMPONENT Runtime
  )

#===DEADZONE===================================================================
endif(UFC_ENABLE_PYTHON)

#------------------------------------------------------------------------------
# Generate UFCConfig.cmake file

configure_file(${UFC_CMAKE_DIR}/UFCConfig.cmake.in UFCConfig.cmake @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/UFCConfig.cmake
  DESTINATION ${UFC_CMAKE_CONFIG_DIR}
  COMPONENT Development
  )

#------------------------------------------------------------------------------
# Generate UFCConfigVersion.cmake file

configure_file(${UFC_CMAKE_DIR}/UFCConfigVersion.cmake.in
  UFCConfigVersion.cmake @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/UFCConfigVersion.cmake
  DESTINATION ${UFC_CMAKE_CONFIG_DIR}
  COMPONENT Development
  )

#------------------------------------------------------------------------------
# Generate pkg-config config file (ufc-1.pc)

configure_file(${UFC_CMAKE_DIR}/ufc-1.pc.in ufc-1.pc @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/ufc-1.pc
  DESTINATION ${UFC_PKGCONFIG_DIR}
  COMPONENT Development
  )

#------------------------------------------------------------------------------
