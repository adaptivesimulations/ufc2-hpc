# Code generation format strings for UFC (Unified Form-assembly Code) v. 2.2.0.
# This code is released into the public domain.
#
# The FEniCS Project (http://www.fenicsproject.org/) 2006-2013.

dofmap_combined = """\
/// This class defines the interface for a local-to-global mapping of
/// degrees of freedom (dofs).

class %(classname)s: public ufc::dofmap
{%(members)s
public:

  /// Constructor
  %(classname)s(%(constructor_arguments)s)
    : ufc::dofmap()%(initializer_list)s
  {
%(constructor)s
  }

  /// Destructor
  ~%(classname)s()
  {
%(destructor)s
  }

  /// Return a string identifying the dofmap
  inline const char* signature() const
  {
%(signature)s
  }

  /// Return true iff mesh entities of topological dimension d are needed
  inline bool needs_mesh_entities(unsigned int d) const
  {
%(needs_mesh_entities)s
  }

  /// Initialize dofmap for mesh (return true iff init_cell() is needed)
  inline bool init_mesh(const ufc::mesh& m)
  {
%(init_mesh)s
  }

  /// Initialize dofmap for given cell
  inline void init_cell(const ufc::mesh& m,
                         const ufc::cell& c)
  {
%(init_cell)s
  }

  /// Finish initialization of dofmap for cells
  inline void init_cell_finalize()
  {
%(init_cell_finalize)s
  }

  /// Return the topological dimension of the associated cell shape
  inline unsigned int topological_dimension() const
  {
%(topological_dimension)s
  }

  /// Return the geometric dimension of the associated cell shape
  inline unsigned int geometric_dimension() const
  {
%(geometric_dimension)s
  }

  /// Return the dimension of the global finite element function space
  inline unsigned int global_dimension() const
  {
%(global_dimension)s
  }

  /// Return the dimension of the local finite element function space for a cell
  inline unsigned int local_dimension() const
  {
%(local_dimension)s
  }

  /// Return the number of dofs on each cell facet
  inline unsigned int num_facet_dofs() const
  {
%(num_facet_dofs)s
  }

  /// Return the number of dofs associated with each cell entity of dimension d
  inline unsigned int num_entity_dofs(unsigned int d) const
  {
%(num_entity_dofs)s
  }

  /// Tabulate the local-to-global mapping of dofs on a cell
  inline void tabulate_dofs(unsigned int* dofs,
                            const ufc::mesh& m,
                            const ufc::cell& c) const
  {
%(tabulate_dofs)s
  }

  /// Tabulate the local-to-local mapping from facet dofs to cell dofs
  void tabulate_facet_dofs(unsigned int* dofs,
                                   unsigned int facet) const
  {
%(tabulate_facet_dofs)s
  }

  /// Tabulate the local-to-local mapping of dofs on entity (d, i)
  void tabulate_entity_dofs(unsigned int* dofs,
                                    unsigned int d, unsigned int i) const
  {
%(tabulate_entity_dofs)s
  }

  /// Tabulate the coordinates of all dofs on a cell
  void tabulate_coordinates(double** coordinates,
                                    const ufc::cell& c) const
  {
%(tabulate_coordinates)s
  }

  /// Return the number of sub dofmaps (for a mixed element)
  inline unsigned int num_sub_dofmaps() const
  {
%(num_sub_dofmaps)s
  }

  /// Create a new dofmap for sub dofmap i (for a mixed element)
  inline ufc::dofmap* create_sub_dofmap(unsigned int i) const
  {
%(create_sub_dofmap)s
  }

  /// Create a new class instance
  inline ufc::dofmap* create() const
  {
%(create)s
  }

};
"""

dofmap_header = """\
/// This class defines the interface for a local-to-global mapping of
/// degrees of freedom (dofs).

class %(classname)s: public ufc::dofmap
{%(members)s
public:

  /// Constructor
  %(classname)s(%(constructor_arguments)s)
    : ufc::dofmap()%(initializer_list)s
  {
%(constructor)s
  }

  /// Destructor
  ~%(classname)s()
  {
%(destructor)s
  }

  inline const char* signature() const
  {
%(signature)s
  }

  /// Return true iff mesh entities of topological dimension d are needed
  inline bool needs_mesh_entities(unsigned int d) const
  {
%(needs_mesh_entities)s
  }

  /// Initialize dofmap for mesh (return true iff init_cell() is needed)
  inline bool init_mesh(const ufc::mesh& m)
  {
%(init_mesh)s
  }

  /// Initialize dofmap for given cell
  inline void init_cell(const ufc::mesh& m,
                         const ufc::cell& c)
  {
%(init_cell)s
  }

  /// Finish initialization of dofmap for cells
  inline void init_cell_finalize()
  {
%(init_cell_finalize)s
  }

  /// Return the topological dimension of the associated cell shape
  inline unsigned int topological_dimension() const
  {
%(topological_dimension)s
  }

  /// Return the geometric dimension of the associated cell shape
  inline unsigned int geometric_dimension() const
  {
%(geometric_dimension)s
  }

  /// Return the dimension of the global finite element function space
  inline unsigned int global_dimension() const
  {
%(global_dimension)s
  }

  /// Return the dimension of the local finite element function space for a cell
  inline unsigned int local_dimension() const
  {
%(local_dimension)s
  }

  /// Return the number of dofs on each cell facet
  inline unsigned int num_facet_dofs() const
  {
%(num_facet_dofs)s
  }

  /// Return the number of dofs associated with each cell entity of dimension d
  inline unsigned int num_entity_dofs(unsigned int d) const
  {
%(num_entity_dofs)s
  }

  /// Tabulate the local-to-global mapping of dofs on a cell
  inline void tabulate_dofs(unsigned int* dofs,
                            const ufc::mesh& m,
                            const ufc::cell& c) const
  {
%(tabulate_dofs)s
  }

  /// Tabulate the local-to-local mapping from facet dofs to cell dofs
  void tabulate_facet_dofs(unsigned int* dofs,
                                   unsigned int facet) const;

  /// Tabulate the local-to-local mapping of dofs on entity (d, i)
  void tabulate_entity_dofs(unsigned int* dofs,
                                    unsigned int d, unsigned int i) const;

  /// Tabulate the coordinates of all dofs on a cell
  void tabulate_coordinates(double** coordinates,
                                    const ufc::cell& c) const;

  /// Return the number of sub dofmaps (for a mixed element)
  inline unsigned int num_sub_dofmaps() const
  {
%(num_sub_dofmaps)s
  }

  /// Create a new dofmap for sub dofmap i (for a mixed element)
  inline ufc::dofmap* create_sub_dofmap(unsigned int i) const
  {
%(create_sub_dofmap)s
  }

  /// Create a new class instance
  inline ufc::dofmap* create() const
  {
%(create)s
  }

};
"""

dofmap_implementation = """\
/// Tabulate the local-to-local mapping from facet dofs to cell dofs
void %(classname)s::tabulate_facet_dofs(unsigned int* dofs,
                                        unsigned int facet) const
{
%(tabulate_facet_dofs)s
}

/// Tabulate the local-to-local mapping of dofs on entity (d, i)
void %(classname)s::tabulate_entity_dofs(unsigned int* dofs,
                                  unsigned int d, unsigned int i) const
{
%(tabulate_entity_dofs)s
}

/// Tabulate the coordinates of all dofs on a cell
void %(classname)s::tabulate_coordinates(double** coordinates,
                                         const ufc::cell& c) const
{
%(tabulate_coordinates)s
}

"""
