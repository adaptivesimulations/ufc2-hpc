# Code generation format strings for UFC (Unified Form-assembly Code) v. 2.2.0.
# This code is released into the public domain.
#
# The FEniCS Project (http://www.fenicsproject.org/) 2006-2013.

form_combined = """\
/// This class defines the interface for the assembly of the global
/// tensor corresponding to a form with r + n arguments, that is, a
/// mapping
///
///     a : V1 x V2 x ... Vr x W1 x W2 x ... x Wn -> R
///
/// with arguments v1, v2, ..., vr, w1, w2, ..., wn. The rank r
/// global tensor A is defined by
///
///     A = a(V1, V2, ..., Vr, w1, w2, ..., wn),
///
/// where each argument Vj represents the application to the
/// sequence of basis functions of Vj and w1, w2, ..., wn are given
/// fixed functions (coefficients).

class %(classname)s: public ufc::form
{%(members)s
public:

  /// Constructor
  %(classname)s(%(constructor_arguments)s)
    : ufc::form()%(initializer_list)s
  {
%(constructor)s
  }

  /// Destructor
  ~%(classname)s()
  {
%(destructor)s
  }

  /// Return a string identifying the form
  inline const char* signature() const
  {
%(signature)s
  }

  /// Return the rank of the global tensor (r)
  inline unsigned int rank() const
  {
%(rank)s
  }

  /// Return the number of coefficients (n)
  inline unsigned int num_coefficients() const
  {
%(num_coefficients)s
  }

  /// Return the number of cell domains
  inline unsigned int num_cell_integrals() const
  {
%(num_cell_domains)s
  }

  /// Return the number of exterior facet domains
  inline unsigned int num_exterior_facet_integrals() const
  {
%(num_exterior_facet_domains)s
  }

  /// Return the number of interior facet domains
  inline unsigned int num_interior_facet_integrals() const
  {
%(num_interior_facet_domains)s
  }

  /// Create a new finite element for argument function i
  ufc::finite_element* create_finite_element(unsigned int i) const
  {
%(create_finite_element)s
  }

  /// Create a new dofmap for argument function i
  ufc::dofmap* create_dofmap(unsigned int i) const
  {
%(create_dofmap)s
  }

  /// Create a new cell integral on sub domain i
  inline ufc::cell_integral* create_cell_integral(unsigned int i) const
  {
%(create_cell_integral)s
  }

  /// Create a new exterior facet integral on sub domain i
  inline ufc::exterior_facet_integral* create_exterior_facet_integral(unsigned int i) const
  {
%(create_exterior_facet_integral)s
  }

  /// Create a new interior facet integral on sub domain i
  inline ufc::interior_facet_integral* create_interior_facet_integral(unsigned int i) const
  {
%(create_interior_facet_integral)s
  }

};
"""

form_header = """\
/// This class defines the interface for the assembly of the global
/// tensor corresponding to a form with r + n arguments, that is, a
/// mapping
///
///     a : V1 x V2 x ... Vr x W1 x W2 x ... x Wn -> R
///
/// with arguments v1, v2, ..., vr, w1, w2, ..., wn. The rank r
/// global tensor A is defined by
///
///     A = a(V1, V2, ..., Vr, w1, w2, ..., wn),
///
/// where each argument Vj represents the application to the
/// sequence of basis functions of Vj and w1, w2, ..., wn are given
/// fixed functions (coefficients).

class %(classname)s: public ufc::form
{%(members)s
public:

  /// Constructor
  %(classname)s(%(constructor_arguments)s)
    : ufc::form()%(initializer_list)s
  {
%(constructor)s
  }

  /// Destructor
  ~%(classname)s()
  {
%(destructor)s
  }

  /// Return a string identifying the form
  inline const char* signature() const
  {
%(signature)s
  }

  /// Return the rank of the global tensor (r)
  inline unsigned int rank() const
  {
%(rank)s
  }

  /// Return the number of coefficients (n)
  inline unsigned int num_coefficients() const
  {
%(num_coefficients)s
  }

  /// Return the number of cell domains
  inline unsigned int num_cell_integrals() const
  {
%(num_cell_domains)s
  }

  /// Return the number of exterior facet domains
  inline unsigned int num_exterior_facet_integrals() const
  {
%(num_exterior_facet_domains)s
  }

  /// Return the number of interior facet domains
  inline unsigned int num_interior_facet_integrals() const
  {
%(num_interior_facet_domains)s
  }

  /// Create a new finite element for argument function i
  ufc::finite_element* create_finite_element(unsigned int i) const;

  /// Create a new dofmap for argument function i
  ufc::dofmap* create_dofmap(unsigned int i) const;

  /// Create a new cell integral on sub domain i
  inline ufc::cell_integral* create_cell_integral(unsigned int i) const
  {
%(create_cell_integral)s
  }

  /// Create a new exterior facet integral on sub domain i
  inline ufc::exterior_facet_integral* create_exterior_facet_integral(unsigned int i) const
  {
%(create_exterior_facet_integral)s
  }

  /// Create a new interior facet integral on sub domain i
  inline ufc::interior_facet_integral* create_interior_facet_integral(unsigned int i) const
  {
%(create_interior_facet_integral)s
  }

};
"""

form_implementation = """\
/// Create a new finite element for argument function i
ufc::finite_element* %(classname)s::create_finite_element(unsigned int i) const
{
%(create_finite_element)s
}

/// Create a new dofmap for argument function i
ufc::dofmap* %(classname)s::create_dofmap(unsigned int i) const
{
%(create_dofmap)s
}

"""
